#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

#open(my $in, "arecord |") or die $!;

# --rate=8000 --format=U8 --channels=1

#open(my $out, "| aplay --rate=8000 --format=U8 --channels=1") or die $!;

# at 800 cps, and 8000 samples/s, there are 10 samples/cycle
# there are 0.125 ms per sample and 1.25 ms per cycle.
# at 5 wpm there are 240 ms/element or 192 cycles per element.
# at 10 wpm there are 120 ms/element or 96 cycles per element.
# at 15 wpm there are 80 ms/element or 64 cycles per element.
# at 20 wpm there are 60 ms/element or 48 cycles per element.
# at 25 wpm there are 48 ms/element or 38.4 cycles per element.  
# Okay!


#my @test = map {map {int(sin(6.2831853*$_/60)*10+10)} (1..60)} (1..24);
### 24 ms of 800 Hz

#my $sigs = FT600z3000Q(\@test);

#print scalar(@$sigs)."\n";

sub FT600z3000Q {
	my ($x) = @_;
	# find max
	my $max = 0;
	my $maxi = 0;
	foreach my $i(0..$#$x){
		if($x->[$i] > $max){
			$max = $x->[$i];
			$maxi = $i;
		}
	}
	print "max $max, $maxi\n";
	# calculate 150 masks that but the max at the centre of 
	# a positive section
	my $base = 600;
	my $roof = 3000;
	my $rate = 48000;
	my $fstep = 2**(1/24);
	my $pi = 3.14159265358979;
	my @signals = ();

	for(my $f=$base; $f<=$roof; $f*=$fstep){
		my $signal = 0;
		foreach my $i(0..$#$x){
			# if($f == 1600){
			# 	print join("\t", $i, $x->[$i], 
			# 		cos(2*$pi*$f*($i-$maxi)/$rate))."\n";
			# }
			if(cos(2*$pi*$f*($i-$maxi)/$rate) > 0){
				$signal += $x->[$i];
			}
			else {
				$signal -= $x->[$i];
			}
		}
		printf "%d\t%d\n", $f, $signal;
		push @signals,$signal;
	}
	return \@signals;
}

#checkbimodal([map {rand(),rand()+10} (1..100)]);

sub checkbimodal {
	my ($x) = @_;
	my @x = sort {$a<=>$b} @$x;
# 	my $min = $x[0];
# 	my $max = $x[$#x];
# 	my $q1 = $x[int($#x/4)];
# 	my $q3 = $x[int($#x*3/4)];
# 	my $step = ($q3-$q1)/10;
# #	print "$min $q1 $q3 $max $step\n";
# 	my @bins = ();
# 	for(my $v = $min; $v < $max; $v += $step){
# 		my $count = 0;
# #		print "$v, $min, $max, $step\n";
# 		while(@x && $x[0] < $v+$step){
# 			shift @x;
# 			$count++;	
# 		}
# 		push @bins, $count;
# 	}
#	my @d1 = diffs(@bins);
	my @d1 = diffs(@x);
	print map {"$_\n"} @d1;
}



sub diffs {
	map {$_[$_]-$_[$_-1]} (1..$#_);
}

sub readFT {
	my $spc = 10;
	my $cycles = 10;
	my $window = $cycles * $spc;
	my $recentlength = 800; # cycles
	my @sums = map {ord('d')*$cycles} (1..$spc); # 10 samples per cycle
	my @window = map {ord('d')} (1..$window); # 10 cycles at a time
	my @recentsignal = map {6*$cycles} (1..$recentlength); # a second of signal
	my $averagesignal = 6*$cycles;
	#open(my $in, '<', 'cqmm0jtx.raw') or die $!;
	open(my $in, '<', 'cqmm0jtx-noisy.raw') or die $!;
	#open(my $in, '<', 'cqmm0jtx-noisier.raw') or die $!;
	my $lastsignal = 'u';
	my $signalcount = 0;

	my @lengths = map {100} (1..20);
	my $avlength = 100;

	my $cw = '';

	while(1){
		# 10 samples per cycle...
		my $buf;
		read($in, $buf, $spc) == $spc or last;
		my $min = 255;
		my $max = 0;
		foreach (0..$spc-1){
			$sums[$_] -= shift @window;
			my $new = ord(substr($buf, $_, 1));
			push @window, $new;
			$sums[$_] += $new;
			$min = $sums[$_] if $sums[$_] < $min;
			$max = $sums[$_] if $sums[$_] > $max;
		}
		my $signal = $max - $min;
		$averagesignal -= $recentsignal[0]/scalar(@recentsignal);
		shift @recentsignal;
		push @recentsignal, $signal;
		$averagesignal += $signal/scalar(@recentsignal);

		my $on = $signal > $averagesignal ? 'd' : 'u';
		if($on eq $lastsignal){
			$signalcount++;
		}
		else {
			#print "$lastsignal$signalcount ";
			$avlength -= $lengths[0]/scalar(@lengths);
			shift @lengths;
			push @lengths, $signalcount;
			$avlength += $signalcount/scalar(@lengths);

			if($lastsignal eq 'u' && $signalcount > $avlength*1.8){
				$cw .= ' / ';
				print " / ";
			}
			elsif($lastsignal eq 'u' && $signalcount > $avlength){
				$cw .= ' ';
				print " ";
			}
			elsif($lastsignal eq 'd' && $signalcount > $avlength){
				$cw .= '-';
				print "-";
			}
			elsif($lastsignal eq 'd' && $signalcount <= $avlength){
				$cw .= '.';
				print ".";
			}

			$lastsignal = $on;
			$signalcount = 0;
		}

		#print Dumper \@window, \@sums;
		#print "$min $max $signal $averagesignal\n";

	}
	print "\n",cw2txt($cw),"\n";
}


sub  make_example_file {
	open(my $out, '>', 'cqmm0jtx48k.raw') or die $!;
	my $cpe = 48; # cycles per element
	my @e = txt2cw("CQ CQ CQ MM0JTX MM0JTX MM0JTX K  ");
	for(my $i=0; $i<@e; $i+=2){
		my $d = $e[$i] * $cpe;
		my $u = $e[$i+1] * $cpe;
		my $signal = 
			'OPQSTUVWXXYYZZZZZYYXXWVUTSQPOMLKJHGFEDCCBBAAAAABBCCDEFGHJKLM'
			x $d;
		$signal .= 
			'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
			x $u;
		print $out $signal;
	}
	close($out);
}

make_example_noise();

sub make_example_noise {
	#open(my $out, '>', 'cqmm0jtx-noisy.raw') or die $!;
	my $noisiness = 50;
	open(my $out, '>', "cqmm0jtx48k-noise$noisiness.raw") or die $!;
	my $cpe = 48; # cycles per element
	#open(my $out, '>', "cqmm0jtx48k-noise$noisiness-5wpm.raw") or die $!;
	#my $cpe = 48*4; #              = 5 wpm
	my @e = txt2cw("CQ CQ CQ MM0JTX MM0JTX MM0JTX K  ");
	for(my $i=0; $i<@e; $i+=2){
		my $d = $e[$i] * $cpe;
		my $u = $e[$i+1] * $cpe;
		#my $signal = 'ABDFGGFDBA' x $d;
		my $signal = 
			'OPQSTUVWXXYYZZZZZYYXXWVUTSQPOMLKJHGFEDCCBBAAAAABBCCDEFGHJKLM'
			x $d;
		$signal .= 
			'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
			x $u;
		#$signal .= 'AAAAAAAAAA' x $u;
		foreach my $i(0..length($signal)-1){
			substr($signal, $i, 1, 
				chr(ord(substr($signal, $i, 1)) + 
				#	int(10*rand()-5) ));
					int($noisiness*rand()-5) ));
		}
		print $out $signal;
	}
	#print $out 'A' x 103424;
	close($out);
}

sub cw2txt {
	my $cw = shift;
	my %key = reverse (' '=> '/', qw( 
		A .- B -... C -.-. D -.. E . F ..-. G --. H ....
		I .. J .--- K -.- L .-.. M -- N -. O --- P .--. 
		Q --.- R .-. S ... T - U -.. V -... W .-- X -..-
		Y -.-- Z --.. 1 .---- 2 ..--- 3 ...-- 4 ....-
		5 ..... 6 -.... 7 --... 8 ---.. 9 ----. 0 -----
	));
	my $txt = '';
	foreach my $char(split /\s+/, $cw){
		next unless exists $key{$char};
		$txt .= $key{$char};
	}
	return $txt;
}

sub txt2cw {
	my $txt = shift;
	my %key = (' '=> '/', qw( 
		A .- B -... C -.-. D -.. E . F ..-. G --. H ....
		I .. J .--- K -.- L .-.. M -- N -. O --- P .--. 
		Q --.- R .-. S ... T - U -.. V -... W .-- X -..-
		Y -.-- Z --.. 1 .---- 2 ..--- 3 ...-- 4 ....-
		5 ..... 6 -.... 7 --... 8 ---.. 9 ----. 0 -----
	));
	my @elements = ();
	foreach my $letter(split //, $txt){
		next unless defined $key{$letter};
		foreach my $element(split //, $key{$letter}){
			print "$element";
			if($element eq '.'){
				push @elements, 1, 1;
			}
			elsif($element eq '-'){
				push @elements, 3, 1;
			}
			elsif($element eq '/'){
				# last one is already 3, so add 4 to it...
				$elements[$#elements] += 4;
			}
		}
		# last one is already 1, so add 2 to it...
		$elements[$#elements] += 2;
		print " ";
	}
	print "\n";
	return @elements;
} 


