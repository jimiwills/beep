#!/usr/bin/perl

use strict;
use warnings;
use List::Util qw/min sum/;
use Data::Dumper;

require './cw-codes.pl';

my %cwcode = rcode();
my %qcode = qcode();

# filter the resulting text...
my $filter = join '|', @ARGV;
$filter = '\\w' unless $filter;
$filter = qr/$filter/;

#open(my $in, "perl audioft.pl |") or die $!;
open(my $in, "perl ./audioft.pl |") or die $!;


my @frame = ();
my @lengths = ();
my @dits = ();

my @cw = ();
my @text = ();

my $lastprint = '';

# shortest dit can be 1, longest 12, longest pause ~ 12 * 7 = 84

while(<$in>){
	chomp;
	my @F = split //;
	# initialize:
	if(scalar(@lengths) == 0){
		@lengths = map {[0]} @F;
		@frame = map {[]} @F;
		@cw = map {''} @F;
		@text = map {''} @F;
		@dits = map {100} @F;
	}
	# accumulate:
	foreach (0..$#F){
		my $v = $F[$_] eq '0' ? -1 : 1;
		my $e = $#{$lengths[$_]};
		my $l = $lengths[$_]->[$e];
		if($l == 0){
			$lengths[$_]->[$e] = $v;
		}
		## if in agreement
		# positive ..  increment length
		elsif($l > 0 && $v > 0){
			$lengths[$_]->[$e]++;		
		}
		# negative .. decrement length
		elsif($l < 0 && $v < 0 && $l > -10 * $dits[$_]){
			$lengths[$_]->[$e]--;		
		}
		# disagreement... new length
		else {
			push @{$lengths[$_]}, $v;
			push @{$frame[$_]}, $l; # push previous, 
							# now completed length onto frame
			while(@{$lengths[$_]} > 30){
				shift @{$lengths[$_]}; # don't let it grow too big
			}
			# consume from frame if lengths give us enough info
			if(@{$lengths[$_]} > 20){
				my @l = map {abs $_} @{$lengths[$_]}[0..$e-1]; 
						# (last one probably not complete)
				#my $min = min @l;
				# not thevery min...
				my $min = (sort {$a<=>$b} @l)[3];

				my @d = map {$_ < 2*$min ? $_ : ()} @l;
				my $d = sum(@d)/scalar(@d);
				$dits[$_] = $d; 

				while(@{$frame[$_]}){
					my $x = shift @{$frame[$_]};
					if($x < 0){
						## key up
						$x = abs $x;
						if($x > $d * 2 && $cw[$_]){
							# interpret character
							if(exists $cwcode{$cw[$_]}){
								# append character
								$text[$_] .= $cwcode{$cw[$_]};
							}
							else {
								# append cw
								$text[$_] .= '['.$cw[$_].']';
							}
							$cw[$_] = '';
						}
						if($x > $d * 7){
							# append space
							$text[$_] .= ' ';
							# check for preceeding qcode
							if($text[$_] =~ /\b([A-Z]+)\s*$/
									&& exists $qcode{$1}){
								$text[$_] .= "($qcode{$1}) ";
							}
						}
						# scroll text:
						$text[$_] =~ s/^.*(.{79})$/$1/;
					}
					else {
						## key down
						if($x < $d * 2){
							# append dit
							$cw[$_] .= '.';
						} 
						else {
							# append dah
							$cw[$_] .= '-';
						}
					}
				}
			}

		}
	}
	my $thisprint = join '', map {$_ =~ /$filter/ ? "$_\n" : ()} @text;
	if($thisprint ne $lastprint){
		print $thisprint;
		$lastprint = $thisprint;
	}
}



