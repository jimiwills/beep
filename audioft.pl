#!/usr/bin/perl
use strict;
use warnings;
use Math::FFT;
use Data::Dumper;

$|++;


open(my $pi, "arecord -c 1 -r 48000 -f U8 |") or die $!;
#open(my $pi, '<', 'cqmm0jtx48k-noise50.raw') or die;
#open(my $pi, '<', 'cqmm0jtx-noisy.raw') or die;
binmode($pi);
my $buf;

while(1){
	read($pi, $buf, 1024) == 1024 or last;
	my $data = [map {$_-128} unpack('C*',$buf)];
	my $fft = Math::FFT->new($data);
	my $spectrum = $fft->spctrm(window => 'hamm');
	#my $coeff = $fft->rdft();
	#print Dumper $spectrum;
	print map {$_ >= 100 ? '#' : int($_/10)}  @{$spectrum}[4..80];
	print "\n";
}

