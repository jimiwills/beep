#!perl
#
#
use strict;
use warnings;
use Data::Dumper;
my $codetable = readcode();
#print Dumper $codetable;
my $revcodetable = revcode($codetable);
#print Dumper $revcodetable;

my $text = "Hello!";
print "TI: $text\n";

my $cw = encode($text);
my $ch = generate_channel($cw,5,15); # level, dotlength
#print join(" ", @$ch);
print "CH: ",read_channel($ch),"\n";
addnoise($ch,5);
#print join(" ", @$ch);

my $sm = smooth($ch, 5);

my $cc = read_channel($sm);
print "NC: $cc","\n";
$text = decode($cc);
print "DC: $cc\nDT: $text\n";

sub encode {
	my $text = lc($_[0]);
	my @keys = sort {length $b <=> length $a} keys %$codetable;
	my $pattern = join('|', map {reg_escape($_)} @keys);
#	print $pattern;
    $text =~ s/($pattern)/print "\n!! `$1`\n" unless exists $codetable->{$1}; $codetable->{$1}.' '/ge;
    return $text;
}
sub decode {
	my ($code) = @_;
	my @code = split /\s+/, $code;
	my $t = '';
	foreach my $c(@code){
 		if (exists $revcodetable->{$c}){
			$c = $revcodetable->{$c}
		}
		else {
			$c = "<$c>";
		}
		$t .= $c;
	}
	return $t;
}

sub addnoise {
	my ($ch,$nl) = @_;
	foreach my $i(0..$#$ch){
		$ch->[$i] += $nl * rand();
	}
}

sub smooth {
	my ($ch,$w) = @_;
	my @sm = ();
	my $i=-$w; my $j=0;
	my $t = 0;
	while($i < -1){
		$t += $ch->[$j];
		$j++; $i++;
	}
	while($j<@$ch){
		$t += $ch->[$j];
		$i++;
		$t -= $ch->[$i];
		$j++;
		push @sm, $t/$w;
	}
	return \@sm;
}

sub revcode {
	return {reverse %{$_[0]}};
}

sub reg_escape {
	$_[0] =~ s/([\[\]()|^*.!:\\?+\$])/\\$1/g;
	return $_[0];
}
sub readcode {
	open(my $fh, '<', 'code') or die "$! could not open code";
	my %code = ();
	while(<$fh>){	
		$code{$1} = $2 if /^(.[^=]*)=([.\- \/]+)=(.*)/;
	}
	close($fh);
	return \%code;
}

sub mean {
	my $sum = 0;
	$sum += $_ foreach @_;
	return $sum/scalar(@_);
}
sub median {
	my @s = sort {$a <=> $b} @_;
	if(scalar (@s) % 2){ 
		# length is odd, take the middle one
		# middle is last/2
		return $s[$#s/2];
	}
	else {
		# take mean of middle two
		# middle two are (last+1)/2 and (last-1)/2
		my $b = ($#s-1)/2;
		my $e = $b + 1;
		return mean($s[$b..$e]);
	}
}

sub round137 {
	my $n = shift;
	return 1 if $n < 2;
	return 3 if $n < 5;
	return 7;
}


sub read_channel {
	# this is the hard bit... make sense of a channel...
	my ($channel) = @_;
	# step 0, noise reduction (by smoothing?)
	# step 1, calculate the average
	my $mean = mean(@$channel);
	# step 2, boolean array whether tone is present at each stage or not
	my @b = map {$_ > $mean ? 1 : '-'} @$channel;
	# step 3, calculate lengths of each tone
	my $s = join('',@b);
	my @s = split(/(1+)/,$s);
	my @ls = map {length($_)} @s;
	my @l = map {/1/ ? length($_) : ()} @s;
	# step 4, calculate average and median lengths
	my $meanl = mean(@l);
	my $medl = median(@l);
	# step 5, if median > average then median is length of a dah, divide by 3 for length of dit
	#         else median is the length of a dit
	my $ditl = $medl > $meanl ? $medl/3 : $medl;
	# step 6, divide lengths by length of dit
	my @n = map {$_/$ditl} @ls;
	# step 7, round lengths to nearest 1, 3 or 7.
	my @r = map {round137($_)} @n;
	# step 8 convert to text
	my $code = '';
	foreach my $i(0..$#r){
		if($i % 2){ # tone
			if($r[$i] == 1){
				$code .= '.';
			}
			else {
				$code .= '-';
			}
		}
		else {
			if($r[$i] == 3){
				$code .= ' ';
			}
			elsif($r[$i] == 7){
				$code .= ' / ';
			}
		}
	}
	return $code;
}

sub generate_channel {
	my ($code,$lvl,$dl) = @_;
	# each automatically has a rest of one after, so 
	# . is 1 + 1
	# - is 3 + 1
	#   is 1 + 1 (plus the 1 from before = 3)
	# / is 1 + 1 (plus the 3 from before and 2 after is 7)
	my @channel = ();
	#my %d = ('.' => 1, '-' => 3, ' ' => 1, '/' = );
	foreach (split //, $code){
		my $v = /\.|-/ ? $lvl : 0;
		my $d = $_ eq '-' ? 3*$dl : $dl;
		foreach(1..$d){
			push @channel, $v;
		}
		foreach(1..$dl){
			push @channel, 0;
		}
	}
	return \@channel;
}


