
sub code {
	return qw{
		a .-		b -...		c -.-.		d -..		e .		f ..-.		g --.
		h ....		i ..		j .---		k -.-		l .-..		m --		n -.
		o ---		p .--.		q --.-		r .-.		s ...		t -
		u ..-		v ...-		w .--		x -..-		y -.--		z --..
		. .-.-.-		, --..--		? ..--..		' .----.
		! -.-.--		/ -..-.		( -.--.		) -.--.-		& .-...
		: ---...		; -.-.-.		= -...-		+ .-.-.		- -....-
		_ ..--.-		" .-..-.		$ ...-..-		@ .--.-.
		1 .----		2 ..---		3 ...--		4 ....-
		5 .....		6 -....		7 --...		8 ---..
		9 ----.		0 -----
	};
}

sub rcode {
	return reverse qw{
		a .-		b -...		c -.-.		d -..		e .		f ..-.		g --.
		h ....		i ..		j .---		k -.-		l .-..		m --		n -.
		o ---		p .--.		q --.-		r .-.		s ...		t -
		u ..-		v ...-		w .--		x -..-		y -.--		z --..
		. .-.-.-		, --..--		? ..--..		' .----.
		! -.-.--		/ -..-.		( -.--.		) -.--.-		& .-...
		: ---...		; -.-.-.		= -...-		+ .-.-.		- -....-
		_ ..--.-		" .-..-.		$ ...-..-		@ .--.-.
		1 .----		2 ..---		3 ...--		4 ....-
		5 .....		6 -....		7 --...		8 ---..
		9 ----.		0 -----
	};
}


sub qcode {
	return (
		'AA' =>'All After',
		'AB' =>'All Before',
		'ABT' =>'About',
		'ADEE' =>'Addressee',
		'ADR' =>'Address',
		'AGN' =>'Again',
		'AM' =>'Amplitude Modulation',
		'ANT' =>'Antenna',
		'BCI' =>'Broadcast Interference',
		'BCL' =>'Broadcast listener',
		'BCNU' =>'Be seeing you',
		'BK' =>'Break in',
		'BN' =>'Between, Been',
		'BT' =>'Separation',
		'BTR' =>'Better',
		'Bug' =>'Semi automatic key',
		'C' =>'Yes, Correct',
		'CFM' =>'Confirm, I confirm',
		'CK' =>'Check',
		'CKT' =>'Circuit',
		'CL' =>'Closing Station, Call',
		'CLBK' =>'Callbook',
		'CLD' =>'Called',
		'CLG' =>'Calling',
		'CNT' =>'Cant',
		'CONDX' =>'Conditions',
		'CQ' =>'Calling any station',
		'CU' =>'See you',
		'CUL' =>'See you later',
		'CUM' =>'Come',
		'CW' =>'Continuous Wave',
		'DA' =>'day',
		'DE' =>'From, From this',
		'DIFF' =>'Difference',
		'DLD' =>'Delivered',
		'DN' =>'Down',
		'DR' =>'Delivered',
		'DX' =>'Distance',
		'EL' =>'Element',
		'ES' =>'And',
		'FB' =>'Fine business',
		'FER' =>'For',
		'FM' =>'Frequency Modulation, From',
		'GA' =>'Go ahead, Good afternoon',
		'GB' =>'Goodbye, God Bless',
		'GD' =>'Good',
		'GE' =>'Good Evening',
		'GESS' =>'Guess',
		'GG' =>'Going',
		'GM' =>'Good Morning',
		'GN' =>'Good Night',
		'GND' =>'Ground',
		'GUD' =>'Good',
		'GV' =>'Give',
		'HH' =>'Error sending',
		'HI HI' =>'Laughter',
		'HR' =>'Hear',
		'HV' =>'Have',
		'HW' =>'How, Copy?',
		'IMI' =>'Repeat, say again',
		'LNG' =>'long',
		'LTR' =>'Later',
		'LVG' =>'Leaving',
		'MA' =>'Milliamperes',
		'MSG' =>'Message',
		'N' =>'No, Nine',
		'NCS' =>'Net Control Station',
		'ND' =>'Nothing Doing',
		'NM' =>'No More',
		'NR' =>'Number',
		'NW' =>'Now , Resume transmission',
		'OB' =>'Old Boy',
		'OC' =>'Old Chap',
		'OM' =>'Old Man',
		'OP' =>'Operator',
		'OPR' =>'Operator',
		'OT' =>'Old Timer',
		'PBL' =>'Preamble',
		'PKG' =>'Package',
		'PSE' =>'Please',
		'PT' =>'Point',
		'PWR' =>'Power',
		'PX' =>'Press',
		'R' =>'Received, Are',
		'RC' =>'Ragchew',
		'RCD' =>'Received',
		'RCVR' =>'Receiver',
		'REF' =>'Refer to',
		'RFI' =>'Radio Frequency Interference',
		'RIG' =>'Station Equipment',
		'RPT' =>'Repeat, Report',
		'RTTY' =>'Radioteletype',
		'RST' =>'Readability Strength Tone',
		'RX' =>'Receive, receiver',
		'SASE' =>'Self addressed stamped envelope',
		'SED' =>'Said',
		'SEZ' =>'Says',
		'SGD' =>'Signed',
		'SIG' =>'Signature, Signal',
		'SINE' =>'Personal initials or nickname',
		'SKED' =>'Schedule',
		'SRI' =>'Sorry',
		'SS' =>'Sweepstakes',
		'SSB' =>'Single Sideband',
		'STN' =>'Station',
		'DLVD' =>'Delivered',
		'SVC' =>'Service',
		'T' =>'Zero',
		'TFC' =>'Traffic',
		'TMW' =>'Tomorrow',
		'TKS' =>'Thanks',
		'TR' =>'Transmit',
		'T' =>'Transmit',
		'TRIX' =>'Tricks',
		'TT' =>'That',
		'TTS' =>'That is',
		'TU' =>'Thank you',
		'TVI' =>'Television interference',
		'TX' =>'Transmitter, Transmit',
		'TXT' =>'text',
		'U' =>'You',
		'UR' =>'You\'re/Your',
		'URS' =>'Yours',
		'VFB' =>'Very Fine Business',
		'VFO' =>'Variable Frequency Oscillator',
		'VY' =>'Very',
		'W' =>'Watts',
		'WA' =>'Word After',
		'WD' =>'Word',
		'WDS' =>'Words',
		'WKD' =>'Worked',
		'WKG' =>'Working',
		'WPM' =>'Words per minute',
		'WRD' =>'Word',
		'MILLS' =>'Milliamperes',
		'TXVR' =>'Transceiver',
		'XMTR' =>'Transmitter',
		'XTL' =>'Crystal',
		'XYL, YF' =>'Wife',
		'YL' =>'Young Lady',
		'YR' =>'Year',
		'73' =>'Best Regards',
		'QRG' =>'Exact frequency',
		'QRI' =>'Tone (T in the RST code)',
		'QRK' =>'Intelligibility (R in the RST code)',
		'QRL' =>'This frequency is busy.',
		'QRM' =>'Man-made interference',
		'QRN' =>'Natural interference, e.g. static crashes',
		'QRO' =>'Increase power',
		'QRP' =>'Decrease power',
		'QRQ' =>'Send more quickly',
		'QRR' =>'Temporarily unavailable/away, please wait',
		'QRS' =>'Send more slowly',
		'QRT' =>'Stop sending',
		'QRU' =>'Have you anything for me?',
		'QRV' =>'I am ready',
		'QRX' =>'Will call you again',
		'QRZ' =>'Who\'s calling',
		'QSA' =>'Signal strength',
		'QSB' =>'Fading of signal',
		'QSD' =>'Your keying is defective',
		'QSK' =>'Break-in',
		'QSL' =>'Acknowledge receipt',
		'QSM' =>'Repeat last message',
		'QSN' =>'I heard you',
		'QSO' =>'A conversation',
		'QSP' =>'Relay',
		'QST' =>'General call to all stations',
		'QSX' =>'I am listening on ... frequency',
		'QSY' =>'Shift to transmit on ...',
		'QTA' =>'Disregard last message',
		'QTC' =>'Traffic',
		'QTH' =>'Location',
		'QTR' =>'Exact time'
	);
}



1;
