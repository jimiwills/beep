#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

my %code = code();
my ($qcodes, $abbrs) = read_data();
my $freq = 600;
my $wpm = 20;

=pod

function...

1) Practise copying/learning word sounds
play a word/abbreviation...
user types in what they copied
if it's right, play another
if it's wrong, play it again and see if they get it right this time

2) Learn q codes and abbreviations...
like 1), but print the meaning so the user can read that while/after hearing

3) randomly generated strings
like 1) but more about the individual letters rather than the words.

=cut

#playprintcw('cq cq cq');
#playprintcw('the quick brown fox jumps over the lazy dog.');

# print Dumper $qcodes;
# print Dumper $abbrs;

test_codes(%$qcodes, %$abbrs);

sub test_codes {
	my $printmeaning = 0;
	if(@_ % 2){
		$printmeaning = pop @_;
	}
	my %codes = @_;
	my @keys = keys %codes;
	my $units = 0;
	my $atts = 0;
	my $codes = 0;
	my $start = time();
	while(1){
		$codes++;
		my $a = '';
		my $i = int(scalar(@keys)*rand());
		my $k = $keys[$i];
		my $m = $codes{$k};
		$k =~ s/\s+//g;
		if($printmeaning){
			$m =~ s/$k/***/gi;
			$m =~ s/[\n\r]+//g;
			print "$m\n";
		}
		while(uc $a ne uc $k){
			last if $a eq 'QUIT';
			$atts ++;
			$units += cw($k);
			$a = <STDIN>;
			$a =~ s/\s+//g;
		}
		last if uc $a eq 'QUIT';
	}
	my $acc = int (100 * $codes / $atts);
	my $durs = time() - $start;
	my $mwpm = int(1.2 * $units / $durs);
	print "$codes / $atts; $acc \% \n$mwpm / $wpm wpm; $units units in $durs s \n";
}

sub playprintcw {
	my $text = shift;
	my @words = split /\s+/, $text;
	my $code = '';
	foreach my $word(@words){
		foreach my $char(split //, $word){
			die "uncodable character: $char" unless exists $code{$char};
			$code = $code{lc $char}.' ';
			print $char;
			aplay(pattern($freq,$wpm,$code));
		}
		$code = '/ ';
		print " ";
		aplay(pattern($freq,$wpm,$code));
	}
}

sub cw {
	my $text = shift;
	$text = tocode($text);
	my $patt = pattern($freq, $wpm, $text);
	aplay($patt);
	my %d = qw{ . 2 - 4 / 6};
	my $s = 0;
	while($text =~ /\G\s*(\S)/g){
		#print "$1";
		die "nothing defined for '$1'" unless exists $d{$1};
		$s += $d{$1};
	}
	return $s;
}

sub tocode {
	my $text = lc shift;
	my @words = split /\s+/, $text;
	my $code = '';
	foreach my $word(@words){
		foreach my $char(split //, $word){
			die "uncodable character: $char" unless exists $code{$char};
			$code .= $code{$char}.' ';
		}
		$code .= '/ ';
	}
	return $code;
}

sub code {
	return qw{
		a .-
		b -...
		c -.-.
		d -..
		e .
		f ..-.
		g --.
		h ....
		i ..
		j .---
		k -.-
		l .-..
		m --
		n -.
		o ---
		p .--.
		q --.-
		r .-.
		s ...
		t -
		u ..-
		v ...-
		w .--
		x -..-
		y -.--
		z --..
		. .-.-.-
		, --..--
		? ..--..
		' .----.
		! -.-.--
		/ -..-.
		( -.--.
		) -.--.-
		& .-...
		: ---...
		; -.-.-.
		= -...-
		+ .-.-.
		- -....-
		_ ..--.-
		" .-..-.
		$ ...-..-
		@ .--.-.
		1 .----
		2 ..---
		3 ...--
		4 ....-
		5 .....
		6 -....
		7 --...
		8 ---..
		9 ----.
		0 -----
	};
}

sub aplay {
	my $wave = shift;
	open(my $aplay, '| aplay -t raw -c 1 -f U8 -r 8000 -q') or die $!;
	binmode($aplay);
	print $aplay $wave;
	close($aplay)
}

sub wpm {
	# 50 units per word (including the space at the end)
	# wpm is words per minute
	# upw*wpm = units per minute
	# 60/upm = seconds per unit?
	my ($wpm) = @_;
	my $upm = 50*$wpm;
	my $spu = 60/$upm;
	# or spu = 1.2/$wpm
	return $spu;
}

sub pattern {
	my ($freq, $wpm, $pattern) = @_;
	my $unitlength = 1.2/$wpm;
	my $wave = '';
	foreach my $char(split /\s+/, $pattern){
		foreach my $tone(split //, $char){
			my $L = 0;
			my $v = 0.2;
			if($tone eq '.'){ $L=1; }
			elsif($tone eq '-'){ $L=3; }
			elsif($tone eq '/'){ $L=1; $v = 0; }
			$wave .= wave($freq, 8000, $L*$unitlength, 10, 10, $v, 0.01);
			$wave .= wave($freq, 8000, $unitlength, 10, 10, 0.0, 0.01);
		}
		$wave .= wave($freq, 8000, 2 * $unitlength, 10, 10, 0.0, 0.01);
	}
	return $wave;
}

sub wave {
	# TODO: put all these into a hash with defaults...
	# then we can report the hash if necessary

	# this is MONO UNSIGNED CHAR
	# use with aplay -t raw -
	my ($freq, $rate, $duration, $fade_in_ms, $fade_out_ms, $mag, $tolerance) = @_;
	# the duration is in seconds, the rate is in samples per second... 
	# so the number_of_samples is $rate * $duration
	my $number_of_samples = $rate * $duration;
	# frequency is cycles per second, and rate is samples per second...
	# so samples_per_cycle is $rate/$freq
	my $samples_per_cycle = $rate/$freq;
	# the number of cycles needed to make the wave is
	my $number_of_cycles = int($freq/$duration)+1;
	# mag is the magnitude, from 0 to 1, but needs to be from 0..127
	$mag *= 127;
	# so a wave would be $mag * sin(2*$PI*$_/$samples_per_cycle)
	my $PIx2 = 8 * atan2(1,1);
	my $rotation_factor = $PIx2/$samples_per_cycle;
	# tolerance is the % tolerance for the frequency... basically the remainder of $samples_per_cycle over samples_per_cycle
	# we can multiply up the number of cycles in a unit to get the tolerance down...
	# since we've already calculated $rotation_factor (which determines the frequency) it is now okay to change
	# $samples_per_cycle and $number_of_cycles
	# tolerance is a proportion (between 0 and 1)
	my $cycles_per_unit = 1;
	while(($cycles_per_unit * $samples_per_cycle-int($cycles_per_unit * $samples_per_cycle))/($cycles_per_unit * $samples_per_cycle) > $tolerance){
		$cycles_per_unit++;
	}
	my $samples_per_unit = $samples_per_cycle * $cycles_per_unit;
	my $number_of_units = $number_of_cycles / $cycles_per_unit;
	# these also need to be integers
	$samples_per_unit = int $samples_per_unit;
	$number_of_units = 1 + int $number_of_units;

	# fadeinms and fadeoutms is the duration in milliseconds of the fade-in and fade-out
	# which makes the start/stop sound a bit nicer
	# the $fade_in_samples is $rate * $fade_in_ms / 1000
	my $fade_in_samples = $rate * $fade_in_ms / 1000;
	my $fade_out_samples = $rate * $fade_out_ms / 1000;

	# make the wave... one cycle
	my $one_unit = pack("C*", map {int(128 + $mag * sin($rotation_factor * $_))} (1..$samples_per_unit));
	# make the number required...
	my $wave = $one_unit x $number_of_units;
	# trim it to the right length
	$wave = substr($wave, 0, $number_of_samples);

	# TODO: face in/out


	# print "Freq: $freq\nRate: $rate\nDuration: $duration\nMag: $mag\n";
	# print "number_of_samples: $number_of_samples\n";
	# print "samples_per_cycle: $samples_per_cycle\n";
	# print "samples_per_unit: $samples_per_unit\n";
	# print "number_of_cycles: $number_of_cycles\n";
	# print "number_of_units: $number_of_units\n";
	# print "rotation_factor: $rotation_factor\n";
	# print "cycles_per_unit: $cycles_per_unit\n";
	# print "one_unit: ",unpack('H*',$one_unit),"\n";

	my @head = map {$_-128} unpack('C*', substr($wave,0,$fade_in_samples));
	my $L = length($wave);
	my @tail = map {$_-128} unpack('C*', substr($wave,$L-$fade_out_samples,$fade_out_samples));
	for(0..$fade_in_samples-1){
		$head[$_] *= $_/$fade_in_samples;
		$tail[$_] *= ($fade_in_samples-1-$_)/$fade_in_samples;
	}
	substr($wave,0,$fade_in_samples, pack('C*', map {$_+128} @head));
	substr($wave,$L-$fade_out_samples,$fade_out_samples, pack('C*', map {$_+128} @tail));

	#print "wavestart: ".unpack('H*',substr($wave,0,40)),"\n";
	#print "waveend: ".unpack('H*',substr($wave,$L-40,40)),"\n";

	return $wave;
}

sub read_data {
	my %q = ();
	my %a = ();
	while(<DATA>){
		last if /---------/;
		my ($q,$m,$e) = split /\t/;
		$q{$q} = "$m, e.g: $e";
	}
	while(<DATA>){
		s/[\n\r]+//g;
		%a = (%a, split /\t/);
	}
	return (\%q, \%a);
}


# http://www.amateur-radio-wiki.net/index.php?title=Codes_and_Alphabets
__DATA__
QRG 	Exact frequency 	HE TX ON QRG 14205 kHz
QRI 	Tone (T in the RST code) 	UR QRI IS 9
QRK 	Intelligibility (R in the RST code) 	UR QRK IS 5
QRL 	This frequency is busy. 	Used almost exclusively with morse code, usually as a question (QRL? - is this frequency busy?) before transmitting on a new frequency
QRM 	Man-made interference 	ANOTHER QSO UP 2 kHz CAUSING LOT OF QRM
QRN 	Natural interference, e.g. static crashes 	BAND NOISY TODAY LOT OF QRN
QRO 	Increase power 	NEED QRO WHEN PROP POOR
QRP 	Decrease power 	QRP TO 5 W (As a mode of operation, a QRP station is five watts or less, a QRPp station one watt or less)
QRQ 	Send more quickly 	TIME SHORT PSE QRQ
QRR 	Temporarily unavailable/away, please wait 	WILL BE QRR 30 MIN = THAT STN IS QRR NW
QRS 	Send more slowly 	PSE QRS NEW TO CW (QRS operation - a slower dot rate - is useful during weak-signal conditions; a QRSS mode uses an extremely low code rate on a channel less than 1Hz wide to allow reception under extreme QRP conditions)
QRT 	Stop sending 	ENJOYED TALKING 2 U = MUST QRT FER DINNER NW
QRU 	Have you anything for me? 	QRU? ABOUT TO QRT
QRV 	I am ready 	WL U BE QRV IN UPCOMING CONTEST?
QRX 	Will call you again 	QRX @ 1500H
QRZ 	You are being called by ________. 	QRZ? UR VY WEAK (Only someone who has previously called should reply)
QSA 	Signal strength 	UR QSA IS 5
QSB 	Fading of signal 	THERE IS QSB ON UR SIG
QSD 	Your keying is defective 	QSD CK YR TX
QSK 	Break-in 	I CAN HR U DURING MY SIGS PSE QSK
QSL 	I Acknowledge receipt 	QSL UR LAST TX = PSE QSL VIA BURO (i.e. please send me a card confirming this contact).
QSM 	Repeat last message 	QRM DROWNED UR LAST MSG OUT = PSE QSM
QSN 	I heard you 	QSN YESTERDAY ON 7005 kHz
QSO 	A conversation 	TNX QSO 73
QSP 	Relay 	PSE QSP THIS MSG TO MY FRIEND
QST 	General call to all stations 	QST: QRG ALLOCS HV CHGD
QSX 	I am listening on ... frequency 	QSX 14200 TO 14210 kHz
QSY 	Shift to transmit on ... 	LETS QSY UP 5 kHz
QTA 	Disregard last message 	QTA, DID NOT MEAN THAT
QTC 	Traffic 	STN WID EMRG QTC PSE GA
QTH 	Location 	QTH IS SOUTH PARK CO
QTR 	Exact time 	QTR IS 2000 Z
--------------
AA 	All After 	OB 	Old Boy
AB 	All Before 	OC 	Old Chap
ABT 	About 	OM 	Old Man
ADEE 	Addressee 	OP 	Operator
ADR 	Address 	OPR 	Operator
AGN 	Again 	OT 	Old Timer
AM 	Amplitude Modulation 	PBL 	Preamble
ANT 	Antenna 	PKG 	Package
BCI 	Broadcast Interference 	PSE 	Please
BCL 	Broadcast listener 	PT 	Point
BCNU 	Be seeing you 	PWR 	Power
BK 	Break in 	PX 	Press
BN 	Between, Been 	R 	Received, Are
BT 	Separation 	RC 	Ragchew
BTR 	Better 	RCD 	Received
Bug 	Semi automatic key 	RCVR 	Receiver
C 	Yes, Correct 	REF 	Refer to
CFM 	Confirm, I confirm 	RFI 	Radio Frequency Interference
CK 	Check 	RIG 	Station Equipment
CKT 	Circuit 	RPT 	Repeat, Report
CL 	Closing Station, Call 	RTTY 	Radioteletype
CLBK 	Callbook 	RST 	Readability Strength Tone
CLD 	Called 	RX 	Receive, receiver
CLG 	Calling 	SASE 	Self addressed stamped envelope
CNT 	Cant 	SED 	Said
CONDX 	Conditions 	SEZ 	Says
CQ 	Calling any station 	SGD 	Signed
CU 	See you 	SIG 	Signature, Signal
CUL 	See you later 	SINE 	Personal initials or nickname
CUM 	Come 	SKED 	Schedule
CW 	Continuous Wave 	SRI 	Sorry
DA 	day 	SS 	Sweepstakes
DE 	From, From this 	SSB 	Single Sideband
DIFF 	Difference 	STN 	Station
DLD	Delivered	DLVD 	Delivered 	SUM 	Some
DN 	Down 	SVC 	Service
DR 	Delivered 	T 	Zero
DX 	Distance 	TFC 	Traffic
EL 	Element 	TMW 	Tomorrow
ES 	And 	TKS 	Thanks	TNX 	Thanks
FB 	Fine business 	TR 	Transmit	TX 	Transmit
FER 	For 	T 	Transmit	R	Receive
FM 	Frequency Modulation, From 	TRIX 	Tricks
GA 	Go ahead, Good afternoon 	TT 	That
GB 	Goodbye, God Bless 	TTS 	That is
GD 	Good 	TU 	Thank you
GE 	Good Evening 	TVI 	Television interference
GESS 	Guess 	TX 	Transmitter, Transmit
GG 	Going 	TXT 	text
GM 	Good Morning 	U 	You
GN 	Good Night 	UR 	You're Your
GND 	Ground 	URS 	Yours
GUD 	Good 	VFB 	Very Fine Business
GV 	Give 	VFO 	Variable Frequency Oscillator
HH 	Error sending 	VY 	Very
HI HI 	Laughter 	W 	Watts
HR 	Hear 	WA 	Word After
HV 	Have 	WD 	Word
HW 	How, Copy? 	WDS 	Words
IMI 	Repeat, say again 	WKD 	Worked
LNG 	long 	WKG 	Working
LTR 	Later 	WPM 	Words per minute
LVG 	Leaving 	WRD 	Word
MA 	Milliamperes 	MILLS 	Milliamperes 	WX 	Weather
MSG 	Message 	TXVR 	Transceiver
N 	No, Nine 	XMTR 	Transmitter
NCS 	Net Control Station 	XTL 	Crystal
ND 	Nothing Doing 	XYL, YF 	Wife
NM 	No More 	YL 	Young Lady
NR 	Number 	YR 	Year
NW 	Now , Resume transmission 	73 	Best Regards 