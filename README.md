# beep

beep is a project about cw (morse code)

Initially, I'm writing in perl, and aiming to make a practice tool unlike any others (that I'm aware of)
It will couple hearing to the keyboard... but will wait for you if you can't keep up.
Will use a Farnsworth-like method, with high wpm for characters, but over-all wpm
dependent on performance.  

It will also help practice recognising and copying words and common abbrs... the 
whole word itself being at high wpm, and over all wpm based on performance.

Currently, I managed to get perl writing to aplay (linux) so I can generate waves.

Next is to make the key and enable converting text to code.  
Then I can start filling in the rest...



# 2017-05-22

Making new stuff now... got FT working for reading audio signal from microphone, so getting a matrix that represents current tones.
Need to copy that script here!

Terminal on windows is 200x75ish.  Need to check on linux.
But if I focus on 75 frequency bins, can do update every second on the last x seconds of code heard... should be decoding more than
one signal at once.

Need to set up vector read-in and matrix maintenance.  Then for each freq bin, process the signal into dot, dahs, and gaps of different
lengths ... call them something or encode somehow?  t1, t3, n1, n3, n7? (for tone and noise/no-tone).

Finally, can output one bin per line.  Either maintain the information for each bin, and side-scroll it, or just process the last x seconds
heard.

Need some way of ignoring frequencies without understandable signals.

