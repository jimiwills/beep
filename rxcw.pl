#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

# read window
# get signal values
# add to frame
# interpret frame when ready
# phase1, initialise timings, translate frame
# phase2, update timings, translate new codes
# 
cw();
#440 880 1760

# what frequencies?
my @f = ();
#for(my $f=600; $f < )

sub cw {
	open(my $in, '<', "cqmm0jtx48k-noise50.raw") or die $!;
	binmode($in);
	my $Mbread = 0;
	my $buf;
	my @frame = ();
	while(1){
		# set up sums
		my @sums = map {0} (0..23);
		foreach my $j(24..80){
			$sums[$j] = [map {0} (1..$j)];
		}
		# read
		last unless read($in, $buf, 480) == 480;
		$Mbread += 0.000480;

# test 
		#$buf = 'OPQSTUVWXXYYZZZZZYYXXWVUTSQPOMLKJHGFEDCCBBAAAAABBCCDEFGHJKLM' x 8;


		# implement sums
		foreach my $i(0..479){
			my $v = ord(substr($buf, $i, 1));
			foreach my $j(24..80){
				$sums[$j]->[$i % $j] += $v;
			}
		}
		# normalise
		foreach my $j(24..80){
			my $d = int(480/$j);
			my $r = 480 % $j;
			foreach my $i(0..$j-1){
				my $di = $d;
				$di++ if $i<$r;
				$sums[$j]->[$i] /= $di;
			}
		}
		# analyse sums
		my @signals = map {0} (0..80);
		foreach my $j(24..80){
			my $min = $sums[$j]->[0];
			my $max = $min;
			my $maxi = 0;
			my $mini = 0;
			foreach my $i (0..$#{$sums[$j]}){
				my $v = $sums[$j]->[$i];
				if($v<$min){
					$min = $v;
					$mini = $i;
				}
				if($v>$max){
					$max = $v;
					$maxi = $i;
				}
			}
			my $posstart = ($maxi + $j*3/4);
			my $negstart = $posstart + $j/2;
			my $sum = 0;
			foreach($posstart..$posstart+$j/2){
				$sum += $sums[$j]->[$_ % $j];
			}
			foreach($negstart..$negstart+$j/2){
				$sum -= $sums[$j]->[$_ % $j];
			}
#			printf "%d  %d  %d %d  %d  %d\n", $j, 48000/$j, $sum, $min, $max, $max-$min;
		}
		#exit;
#		print "\r$Mbread";
	}
}
